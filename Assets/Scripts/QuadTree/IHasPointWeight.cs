﻿namespace QuadTreeLib
{
    public interface IHasPointWeight
    {
        ushort X { get; set; }
        ushort Y { get; set; }
        ushort Weight { get; set; }

        int Compare<T>(T other) where T : IHasPointWeight;
    }
}
