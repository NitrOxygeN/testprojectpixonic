﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QuadTreeLib
{
    public class QuadTreeNode<T> where T : IHasPointWeight
    {
        public Rectangle Bounds { get; private set; }
        public List<T> Contents { get; private set; }
        public int Depth { get; private set; }

        private List<QuadTreeNode<T>> Nodes = new List<QuadTreeNode<T>>(4);

        public QuadTreeNode(Rectangle bounds, int depth)
        {
            Bounds = bounds;
            Depth = depth;
            CreateSubNodes();
        }

        public List<T> Query(Rectangle queryArea, int max)
        {
            // create a list of the items that are found
            List<T> results = new List<T>();

            if (Nodes.Count == 0)
            {
                foreach (T item in Contents)
                {
                    if (queryArea.Contains(item.X, item.Y))
                    {
                        results.Add(item);
                        if (results.Count == max)
                            break;
                    }
                }
            }
            else foreach (QuadTreeNode<T> node in Nodes)
            {
                // Case 1: search area completely contained by sub-quad
                // if a node completely contains the query area, go down that branch
                // and skip the remaining nodes (break this loop)
                if (node.Bounds.Contains(queryArea))
                {
                    results = Merge(results, node.Query(queryArea, max), max);
                    break;
                }

                // Case 2: Sub-quad completely contained by search area 
                // if the query area completely contains a sub-quad,
                // just add all the contents of that quad and it's children 
                // to the result set. You need to continue the loop to test 
                // the other quads
                if (queryArea.Contains(node.Bounds))
                {
                    results = Merge(results, node.Contents.GetRange(0, Math.Min(node.Contents.Count, max)), max);
                    continue;
                }

                // Case 3: search area intersects with sub-quad
                // traverse into this quad, continue the loop to search other
                // quads
                if (node.Bounds.IntersectsWith(queryArea))
                {
                    results = Merge(results, node.Query(queryArea, max), max);
                }
            }

            return results;
        }

        public void Insert(T item)
        {
            // if the item is not contained in this quad, there's a problem
            if (!Bounds.Contains(item.X, item.Y))
            {
                UnityEngine.Debug.LogWarning("feature is out of the bounds of this quadtree node");
                return;
            }

            // for each subnode:
            // if the node contains the item, add the item to that node and return
            // this recurses into the node that is just large enough to fit this item
            foreach (QuadTreeNode<T> node in Nodes)
            {
                if (node.Bounds.Contains(item.X, item.Y))
                {
                    node.Insert(item);
                    return;
                }
            }

            // if we make it to here,
            // we're at the smallest subnode size allowed 
            // add the item to this node's contents.
            if (Contents.Count < Contents.Capacity)
                Contents.Add(item);
        }

        public IEnumerator Sort(Comparison<T> comparison, ushort specialModeMaxPlanets)
        {
            if (Nodes.Count == 0)
            {
                Contents.Sort(comparison);
                LoadingController.Instance.PartSorted(Depth);
            }
            else
            {
                foreach (QuadTreeNode<T> node in this.Nodes)
                {
                    yield return node.Sort(comparison, specialModeMaxPlanets);
                }
                Contents = Merge(Nodes[0].Contents, Nodes[1].Contents, Nodes[2].Contents, Nodes[3].Contents, specialModeMaxPlanets);
            }
        }

        private List<T> Merge(List<T> left, List<T> right, int max)
        {
            int a = 0, b = 0;
            int count = Math.Min(left.Count + right.Count, max);
            int compare = 0;
            List<T> merged = new List<T>(count);
            for (int i = 0; i < count; i++)
            {
                if (b < right.Count && a < left.Count)
                {
                    compare = left[a].Compare(right[b]);
                    if (compare == 0)
                    {
                        if (a <= b)
                            merged.Add(left[a++]);
                        else
                            merged.Add(right[b++]);
                    }
                    else if (compare > 0 && b < right.Count)
                        merged.Add(right[b++]);
                    else
                        merged.Add(left[a++]);
                }
                else
                if (b < right.Count)
                    merged.Add(right[b++]);
                else
                    merged.Add(left[a++]);
            }
            return merged;
        }

        private List<T> Merge(List<T> right1, List<T> right2, List<T> right3, List<T> right4, int max)
        {
            int a = 0, b = 0, c = 0, d = 0;

            int r1 = NextWeight(a, right1);
            int r2 = NextWeight(b, right2);
            int r3 = NextWeight(c, right3);
            int r4 = NextWeight(d, right4);

            int count = Math.Min(right1.Count + right2.Count + right3.Count + right4.Count, max);
            List<T> merged = new List<T>(count);
            for (int i = 0; i < count; i++)
            {
                if (r1 == r2 && r1 == r3 && r1 == r4)
                {
                    if (LessOrEqual(a, b, c, d))
                    {
                        merged.Add(right1[a++]);
                        r1 = NextWeight(a, right1);
                        continue;
                    }
                    if (LessOrEqual(b, a, c, d))
                    {
                        merged.Add(right2[b++]);
                        r2 = NextWeight(b, right2);
                        continue;
                    }
                    if (LessOrEqual(c, a, b, d))
                    {
                        merged.Add(right3[c++]);
                        r3 = NextWeight(c, right3);
                        continue;
                    }
                    if (LessOrEqual(d, a, b, c))
                    {
                        merged.Add(right4[d++]);
                        r4 = NextWeight(d, right4);
                        continue;
                    }
                }
                if (r1 <= r2 && r1 <= r3 && r1 <= r4)
                {
                    merged.Add(right1[a++]);
                    r1 = NextWeight(a, right1);
                    continue;
                }
                if (r2 <= r1 && r2 <= r3 && r2 <= r4)
                {
                    merged.Add(right2[b++]);
                    r2 = NextWeight(b, right2);
                    continue;
                }
                if (r3 <= r1 && r3 <= r2 && r3 <= r4)
                {
                    merged.Add(right3[c++]);
                    r3 = NextWeight(c, right3);
                    continue;
                }
                if (r4 <= r1 && r4 <= r2 && r4 <= r3)
                {
                    merged.Add(right4[d++]);
                    r4 = NextWeight(d, right4);
                    continue;
                }
            }

            return merged;
        }

        private bool LessOrEqual(int a, int b, int c, int d)
        {
            return a <= b && a <= c && a <= d;
        }

        private int NextWeight(int i, List<T> array)
        {
            return i == array.Count ? int.MaxValue : Mathf.Abs(array[i].Weight - Ship.Weight);
        }

        private void CreateSubNodes()
        {
            // the smallest subnode has an area 
            if (Bounds.Height * Bounds.Width <= 390625) // 625 * 625
            {
                Contents = new List<T>(131072); // 2 ^ 17
                return;
            }
            
            ushort halfWidth = (ushort)(Bounds.Width / 2);
            ushort halfHeight = (ushort)(Bounds.Height / 2);
            ushort oddWidth = (ushort)(halfWidth + Bounds.Width % 2);
            ushort oddHeight = (ushort)(halfHeight + Bounds.Height % 2);
            int offsetWidth = Bounds.X + oddWidth;
            int offsetHeight = Bounds.Y + oddHeight;

            Nodes.Add(new QuadTreeNode<T>(new Rectangle(Bounds.X, Bounds.Y, oddWidth, oddHeight), Depth + 1));
            Nodes.Add(new QuadTreeNode<T>(new Rectangle(Bounds.X, offsetHeight, oddWidth, halfHeight), Depth + 1));
            Nodes.Add(new QuadTreeNode<T>(new Rectangle(offsetWidth, Bounds.Y, halfWidth, oddHeight), Depth + 1));
            Nodes.Add(new QuadTreeNode<T>(new Rectangle(offsetWidth, offsetHeight, halfWidth, halfHeight), Depth + 1));
        }
    }
}
