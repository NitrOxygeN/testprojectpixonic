﻿using System.Collections;
using System.Collections.Generic;

namespace QuadTreeLib
{
    /// <summary>
    /// A Quadtree is a structure designed to partition space so
    /// that it's faster to find out what is inside or outside a given 
    /// area. See http://en.wikipedia.org/wiki/Quadtree for more info.
    /// </summary>
    public class QuadTree<T> where T : IHasPointWeight
    {
        QuadTreeNode<T> Root;
        Rectangle Rectangle;

        public QuadTree(Rectangle rectangle)
        {
            Rectangle = rectangle;
            Root = new QuadTreeNode<T>(Rectangle, 0);
        }

        public void Insert(T item)
        {
            Root.Insert(item);
        }

        public List<T> Query(Rectangle area, int max)
        {
            return Root.Query(area, max);
        }

        public IEnumerator Sort(System.Comparison<T> comparison, ushort specialModeMaxPlanets)
        {
            yield return Root.Sort(comparison, specialModeMaxPlanets);
        }
    }
}
