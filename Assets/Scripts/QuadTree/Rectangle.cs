﻿namespace QuadTreeLib
{
    public class Rectangle
    {
        public int X { get; set; }
        public int Y { get; set; }
        public ushort Width { get; set; }
        public ushort Height { get; set; }

        public Rectangle(int x, int y, ushort width, ushort height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        public bool Contains(Rectangle other)
        {
            if (X <= other.X && other.X + other.Width <= X + Width && Y <= other.Y)
                return other.Y + other.Height <= Y + Height;
            return false;
        }

        public bool Contains(ushort otherX, ushort otherY)
        {
            if (X <= otherX && otherX + 1 <= X + Width && Y <= otherY)
                return otherY + 1 <= Y + Height;
            return false;
        }

        public bool IntersectsWith(Rectangle other)
        {
            if (other.X <= X + Width && X <= other.X + other.Width && other.Y <= Y + Height)
                return Y <= other.Y + other.Height;
            return false;
        }
    }
}
