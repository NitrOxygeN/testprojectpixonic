﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using QuadTreeLib;
using TMPro;
using Random = UnityEngine.Random;

public class GridController : MonoBehaviour
{
    class Planet : IHasPointWeight
    {
        public ushort X { get; set; }
        public ushort Y { get; set; }
        public ushort Weight { get; set; }

        private uint color;
        public uint Color { get { if (color == 0) color = PlanetRenderer.GetRandomColor(); return color; } }

        public int Compare<T>(T other) where T : IHasPointWeight
        {
            return Mathf.Abs(this.Weight - Ship.Weight) - Mathf.Abs(other.Weight - Ship.Weight);
        }
    }

    public ushort GridSize = 5;
    [Range(0, 30)] public int PlanetDensity = 30;
    public ushort MaxWeight = 10000;
    public ushort SpecialModeSize = 10;
    public ushort SpecialModeMaxPlanets = 20;

    public Ship Ship = null;
    public ZoomController ZoomController = null;
    public GameObject PlanetPrefab = null;
    private List<PlanetRenderer> PlanetsPool;
    private QuadTree<Planet> Planets;

    IEnumerator Start()
	{
        //initing quadtree
        Planets = new QuadTree<Planet>(new Rectangle(0, 0, GridSize, GridSize));
	    PlanetsPool = new List<PlanetRenderer>(PlanetDensity);
	    ZoomController.gameObject.SetActive(false);
	    Ship.gameObject.SetActive(false);
        LoadingController.Instance.SetLoadingStatus(1);
        yield return null;
        //initing ship weight
        Ship.SetWeight((int)(Random.Range(0f, MaxWeight)));
	    Ship.GridSize = GridSize;
	    LoadingController.Instance.SetLoadingStatus(2);
        yield return null;
        //creating pool of planet prefabs
        for (int i = 0; i < PlanetDensity; i++)
	    {
	        AddNewItemInPool();
	    }
	    LoadingController.Instance.SetLoadingStatus(3);
        yield return null;
        //generating planets in random places with random weight
	    int step = (int)(1f / (PlanetDensity / 100f));
        int count = (int)(GridSize * GridSize * (1f / step));
	    int maxOffset = step - 1;
	    for (int i = 0; i < count; i++)
        {
            float random = Random.Range(0f, 1f);
            int point = i * step + Mathf.Min((int)(step * random), maxOffset);
            Planets.Insert(new Planet() { Weight = (ushort)(MaxWeight * random), X = (ushort)(point % GridSize), Y = (ushort)(point / GridSize) });
            if (i > 0 && i % 1000000 == 0) //yield every 1 mln inserts
            {
                LoadingController.Instance.SetLoadingStatus(3 + (int)((float)i / (float)count * 47f));
                yield return null;
            }
        }
	    LoadingController.Instance.SetLoadingStatus(50);
        //sorting planets by weight and caching top N planets in every node for its child nodes
	    yield return Planets.Sort((a, b) => a.Compare(b), SpecialModeMaxPlanets);
        //finish loading, set player in the center of space and show the game
        ZoomController.SetAction(UpdatePlanets);
	    ZoomController.gameObject.SetActive(true);
        Ship.SetAction(UpdatePlanets);
	    Ship.SetPosition(GridSize / 2, GridSize / 2);
	    Ship.gameObject.SetActive(true);
        LoadingController.Instance.SetLoadingStatus(101);
    }

    private void UpdatePlanets()
    {
        //get size of screen in cells (one cell = 1 point in camera's ort. size)
        int sizeY = Mathf.RoundToInt(Camera.main.orthographicSize);
        int sizeX = Mathf.RoundToInt(Camera.main.orthographicSize * Screen.width / Screen.height);

        //get coordinates of screen in space
        int x1 = Ship.X - sizeX / 2;
        int y1 = Ship.Y - sizeY / 2;
        int x2 = Ship.X + sizeX / 2 + 1;
        int y2 = Ship.Y + sizeY / 2 + 1;

        //is this a special mode?
        int maxPlanets = Camera.main.orthographicSize > SpecialModeSize ? SpecialModeMaxPlanets : int.MaxValue;
        float scale = Camera.main.orthographicSize > SpecialModeSize ? GetSpecialModeScale() : 1f;
        Ship.SetScale(scale);

        int count = 0;
        List<Planet> planets = Planets.Query(new Rectangle(x1, y1, (ushort)(x2 - x1), (ushort)(y2 - y1)), maxPlanets);

        for (int i = 0; i < planets.Count; i++)
        {
            if (count == PlanetsPool.Count)
            {
                AddNewItemInPool();
            }

            PlanetsPool[count].SetWeight(planets[i].Weight);
            PlanetsPool[count].SetColor(planets[i].Color);
            PlanetsPool[count].SetPosition(planets[i].X, planets[i].Y, GridSize);
            PlanetsPool[count].SetScale(scale);
            PlanetsPool[count].gameObject.SetActive(true);
            
            if (++count == maxPlanets)
                break;
        }

        for (int i = count; i < PlanetsPool.Count; i++)
        {
            PlanetsPool[i].gameObject.SetActive(false);
        }
    }

    private float GetSpecialModeScale()
    {
        return (Camera.main.orthographicSize - Camera.main.orthographicSize * 0.5f * ((Camera.main.orthographicSize - SpecialModeSize) / (10000f - SpecialModeSize))) / SpecialModeSize;
    }

    private void AddNewItemInPool()
    {
        GameObject go = Instantiate(PlanetPrefab, transform, false);
        PlanetsPool.Add(go.GetComponent<PlanetRenderer>());
        go.SetActive(false);
    }
}
