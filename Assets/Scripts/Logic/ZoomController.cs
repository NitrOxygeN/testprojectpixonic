﻿using System;
using UnityEngine;

public class ZoomController : MonoBehaviour
{
    public Camera CameraComponent = null;
    private float zoomSize = 5f;

    private Action UpdatePlanets;

	void Start ()
	{
        if (CameraComponent == null)
	        CameraComponent = GetComponent<Camera>();
	}
	
	void Update ()
	{
		if (CameraComponent == null)
            return;

	    float sign = Input.GetAxis("Mouse ScrollWheel");
	    if (Mathf.Abs(sign) > 0f)
	    {
	        sign = Mathf.Sign(sign);
            zoomSize = Mathf.Min(Mathf.Max(5f, zoomSize - sign * zoomSize * 0.2f), 10000f);
	        CameraComponent.orthographicSize = zoomSize;
            if (UpdatePlanets != null)
	            UpdatePlanets();
        }
	}

    public void SetAction(Action action)
    {
        UpdatePlanets = action;
    }
}
