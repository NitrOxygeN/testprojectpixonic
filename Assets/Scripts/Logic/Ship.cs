﻿using System;
using TMPro;
using UnityEngine;

public class Ship : MonoBehaviour
{
    public static int Weight = 9999;
    public int X = 0;
    public int Y = 0;
    public SpriteRenderer Sprite = null;
    public TextMeshPro TextWeight = null;
    public int GridSize = 0;

    private Action UpdatePlanets;
    private Vector2 PrevPosition = Vector2.zero;

    private float nextKeyClickedTime = -1f;
    private const float KEY_DOWN_THRESHOLD = 0.15f;

    public void SetWeight(int value)
    {
        Weight = value;
        TextWeight.text = value.ToString();
    }

    public void SetPosition(int x, int y)
    {
        X = x;
        Y = y;
        UpdatePosition(false);
    }

    public void SetScale(float scale)
    {
        transform.localScale = Vector3.one * scale;
    }

    void Update()
    {
        bool isNeedUpdatePosition = false;
        bool isNeedUpdateRotation = true;

        if (GetKeyClickedOrHold(KeyCode.W))
        {
            Y += (int)Camera.main.orthographicSize / 5;
            if (Y >= GridSize)
            {
                Y = Y % GridSize;
                isNeedUpdateRotation = false;
            }
            isNeedUpdatePosition = true;
        }
        else if (GetKeyClickedOrHold(KeyCode.S))
        {
            Y -= (int)Camera.main.orthographicSize / 5;
            if (Y < 0)
            {
                Y = GridSize - Mathf.Abs(Y % GridSize);
                isNeedUpdateRotation = false;
            }
            isNeedUpdatePosition = true;
        }
        if (GetKeyClickedOrHold(KeyCode.A))
        {
            X -= (int)Camera.main.orthographicSize / 5;
            if (X < 0)
            {
                X = GridSize - Mathf.Abs(X % GridSize);
                isNeedUpdateRotation = false;
            }
            isNeedUpdatePosition = true;
        }
        if (GetKeyClickedOrHold(KeyCode.D))
        {
            X += (int)Camera.main.orthographicSize / 5;
            if (X >= GridSize)
            {
                X = X % GridSize;
                isNeedUpdateRotation = false;
            }
            isNeedUpdatePosition = true;
        }

        if (isNeedUpdatePosition)
        {
            nextKeyClickedTime = Time.time + KEY_DOWN_THRESHOLD;
            UpdatePosition(isNeedUpdateRotation);
        }
    }

    private bool GetKeyClickedOrHold(KeyCode code)
    {
        return Input.GetKey(code) && Time.time > nextKeyClickedTime;
    }

    private void UpdatePosition(bool isNeedUpdateRotation = true)
    {
        transform.parent.localPosition = new Vector3(X * 2 + 1 - GridSize, Y * 2 + 1 - GridSize);
        if (UpdatePlanets != null)
            UpdatePlanets();

        Vector2 currentPosition = new Vector2(X, Y);
        if (isNeedUpdateRotation)
        {
            Vector2 moveDirection = currentPosition - PrevPosition;
            Sprite.transform.eulerAngles = new Vector3(0, 0, Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg - 90f);
        }
        PrevPosition = currentPosition;
    }

    public void SetAction(Action action)
    {
        UpdatePlanets = action;
    }
}
