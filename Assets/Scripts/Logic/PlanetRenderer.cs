﻿using TMPro;
using UnityEngine;

public class PlanetRenderer : MonoBehaviour
{
    public int Weight = 9999;
    public int X = 0;
    public int Y = 0;
    public SpriteRenderer Sprite = null;
    public TextMeshPro TextWeight = null;

    public void SetWeight(int value)
    {
        Weight = value;
        TextWeight.text = value.ToString();
    }

    public void SetColor(uint color)
    {
        Sprite.color = HexToColor(color);
    }

    public void SetPosition(int x, int y, int gridSize)
    {
        X = x;
        Y = y;
        transform.localPosition = new Vector3(X * 2f + 1f - gridSize, Y * 2f + 1f - gridSize);
    }

    public void SetScale(float scale)
    {
        transform.localScale = Vector3.one * scale;
    }

    public static Color HexToColor(uint c)
    {
        float r = ((c & 0xff000000) >> 24);
        float g = ((c & 0x00ff0000) >> 16);
        float b = ((c & 0x0000ff00) >> 8);
        float a = ((c & 0x000000ff) >> 0);

        return new Color(r / 255f, g / 255f, b / 255f, a / 255f);
    }

    public static uint ColorToHex(Color32 c)
    {
        int r = (c.r << 24);
        int g = (c.g << 16);
        int b = (c.b << 8);
        int a = (c.a << 0);

        return (uint)(a | r | g | b);
    }

    public static uint GetRandomColor()
    {
        return ColorToHex(Random.ColorHSV(0f, 1f, 0.75f, 1f, 0.75f, 1f, 1f, 1f));
    }
}